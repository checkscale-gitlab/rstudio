# R and RStudio Desktop/Server packages for the Arch Linux

<!-- badges: start -->
<!-- badges: end -->

## Packages

Package |  License | Version
------- | ---------- | -------
[`r-devel-svn`](https://aur.archlinux.org/packages/r-devel-svn) | ![](https://img.shields.io/aur/license/r-devel-svn) | ![](https://img.shields.io/aur/version/r-devel-svn)
[`r-mkl`](https://aur.archlinux.org/packages/r-mkl) | ![](https://img.shields.io/aur/license/r-mkl) | ![](https://img.shields.io/aur/version/r-mkl)
[`microsoft-r-open`](https://aur.archlinux.org/packages/microsoft-r-open) | ![](https://img.shields.io/aur/license/microsoft-r-open) | ![](https://img.shields.io/aur/version/microsoft-r-open)
[`pandoc-bin`](https://aur.archlinux.org/packages/pandoc-bin) | ![](https://img.shields.io/aur/license/pandoc-bin) | ![](https://img.shields.io/aur/version/pandoc-bin)
[`rstudio-desktop`](https://aur.archlinux.org/packages/rstudio-desktop) |  ![](https://img.shields.io/aur/license/rstudio-desktop) | ![](https://img.shields.io/aur/version/rstudio-desktop)
[`rstudio-desktop-git`](https://aur.archlinux.org/packages/rstudio-desktop-git) | ![](https://img.shields.io/aur/license/rstudio-desktop-git) | ![](https://img.shields.io/aur/version/rstudio-desktop-git)
[`rstudio-server-git`](https://aur.archlinux.org/packages/rstudio-server-git) | ![](https://img.shields.io/aur/license/rstudio-server-git) | ![](https://img.shields.io/aur/version/rstudio-server-git)

See also repo listing: <https://aur1.gitlab.io/rstudio/>

## Usage

### Add repository

Add the following code to `/etc/pacman.conf`:

``` toml
[rstudio]
SigLevel = PackageOptional
Server = https://aur1.gitlab.io/$repo/$arch
```

### List packages

To show actual packages list:

``` bash
pacman -Sl rstudio
```

### Install packages

To install package:

``` bash
pacman -Syy
pacman -S rstudio-desktop-git
```
